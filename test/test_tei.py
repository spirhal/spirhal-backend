from pathlib import Path
from bs4 import BeautifulSoup
from lxml import etree


from backend.hal_to_tei_mappings import add_missing_fields


DATA_PATH = Path(__file__).parent / "data"


def validate_with_xsd(xml_data):
    """Validate an XML file (.xml) according to an XML schema (.xsd)."""
    with open(DATA_PATH / "xsd" / "aofr-sword.xsd") as xsd:
        xmlschema = etree.XMLSchema(etree.parse(xsd))
    # Pretty-print xml_data to get meaningfull line information.
    xml_data = etree.tostring(etree.fromstring(xml_data), pretty_print=True)
    root = etree.fromstring(xml_data)
    if not xmlschema.validate(root):
        print(xml_data)
    xmlschema.assertValid(root)


def test_tei_new_fields():
    with open(DATA_PATH / "tei.xml") as fp:
        tei = fp.read()

    assert "<publisher>" not in tei
    assert "<pubPlace>" not in tei
    assert "<editor>" not in tei
    assert '<biblScope unit="pp">' not in tei

    new_fields = {
        "publicationLocation_s": "Toulouse",
        "page_s": "5-8",
        "scientificEditor_s": "Jean Poulet",
        "publisher_s": "Springer",
    }

    new_tei = add_missing_fields(tei, new_fields, doc_type="COUV")
    validate_with_xsd(new_tei.encode("utf-8"))
    soup = BeautifulSoup(new_tei.replace("\n", ""), features="xml")
    imprint_children = [str(x) for x in soup.imprint.children]
    assert [
        "<publisher>Springer</publisher>",
        "<pubPlace>Toulouse</pubPlace>",
        '<biblScope unit="pp">5-8</biblScope>',
        '<biblScope unit="issue">12</biblScope>',
        '<date type="datePub">2021</date>',
    ] == imprint_children

    assert "<editor>Jean Poulet</editor>" in new_tei


def test_tei_dates_datePub():
    with open(DATA_PATH / "tei_comm_nodate.xml") as fp:
        tei = fp.read()
    soup = BeautifulSoup(tei, features="xml")
    dates = soup.find_all("imprint")[0].find_all("date")
    assert len(dates) == 0
    new_fields = {
        "producedDateY_i": "2019",
    }
    new_tei = add_missing_fields(tei, new_fields, doc_type="COMM")
    soup = BeautifulSoup(new_tei, features="xml")
    dates = soup.find_all("imprint")[0].find_all("date")
    assert len(dates) == 1
    assert str(dates[0]) == '<date type="datePub">2019</date>'


def test_tei_dates_dateDefended():
    with open(DATA_PATH / "tei_hdr_nodate.xml") as fp:
        tei = fp.read()
    soup = BeautifulSoup(tei, features="xml")
    dates = soup.find_all("imprint")[0].find_all("date")
    assert len(dates) == 0
    new_fields = {
        "producedDateY_i": "2019",
    }
    new_tei = add_missing_fields(tei, new_fields, doc_type="HDR")
    soup = BeautifulSoup(new_tei, features="xml")
    dates = soup.find_all("imprint")[0].find_all("date")
    assert len(dates) == 1
    assert str(dates[0]) == '<date type="dateDefended">2019</date>'
