import json

from pathlib import Path
from fastapi.testclient import TestClient

from backend.main import app


DATA_PATH = Path(__file__).parent / "data"


client = TestClient(app)


def test_researcher_list(requests_mock):
    with open(DATA_PATH / "efts.html") as fp:
        requests_mock.get(
            "https://efts.univ-tlse2.fr/accueil/navigation/les-chercheurs/",
            text=fp.read(),
        )
    response = client.get("/api/researcher-list?labo_identifier=EFTS")
    assert response.status_code == 200
    json_response = response.json()
    assert {
        "originalName": "MME Isabelle JOURDAN",
        "link": "https://efts.univ-tlse2.fr/accueil/les-chercheurs/mme-isabelle-jourdan",
        "name": {"dispName": "Jourdan Isabelle", "valName": "Isabelle Jourdan"},
    } in json_response["researchers"]
    assert "Jourdan Isabelle" in json_response["duplicates"]


def test_duplicates(requests_mock):
    url = "https://plh.univ-tlse2.fr/jeanpoulet"
    with open(DATA_PATH / "doublon.html") as fp:
        requests_mock.get(url, text=fp.read())
    request_data = {
        "url": url,
        "notices": [
            {
                "id": "1",
                "title": "Quintus Cornuficius, héritier de l’Africana causa?",
            },
            {"id": "22", "title": "La vie passionnante de Jean Poulet"},
        ],
    }
    response = client.post(
        "/api/duplicate-notices",
        data=json.dumps(request_data),
        headers={"Content-Type": "application/json"},
    )
    assert response.status_code == 200
    json_response = response.json()
    assert ["1"] == json_response


def test_login_ok():
    response = client.post(
        "/api/login",
        json={"login": "jeanpoulet", "password": "loutre"},
    )
    assert response.status_code == 200
    token = response.json()
    assert len(token) > 0

    response = client.get(
        "/api/test-session",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.json()["login"] == "jeanpoulet"


def test_login_ko():
    response = client.get(
        "/api/test-session",
    )
    assert response.status_code == 401

    response = client.post(
        "/api/login",
        json={"login": "jeanpoulet", "password": "loutre"},
    )
    assert response.status_code == 200
    token = response.json()
    assert len(token) > 0

    response = client.get(
        "/api/test-session",
    )
    assert response.status_code == 401


def test_logout():
    response = client.get(
        "/api/test-session",
    )
    assert response.status_code == 401

    response = client.post(
        "/api/login",
        json={"login": "jeanpoulet", "password": "loutre"},
    )
    assert response.status_code == 200
    token = response.json()
    assert len(token) > 0

    response = client.get(
        "/api/logout",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == 200

    response = client.get(
        "/api/test-session",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == 403
