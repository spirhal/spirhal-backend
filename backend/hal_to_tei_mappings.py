from bs4 import BeautifulSoup

HAL_TO_TEI = {
    "title_s": {
        "xpath": "analytic",
        "new_tag": {"tag": "title", "attributes": {"xml:lang": "fr"}},
    },
    "volume_s": {
        "xpath": "imprint",
        "new_tag": {"tag": "biblScope", "attributes": {"unit": "volume"}},
    },
    "issue_s": {
        "xpath": "imprint",
        "new_tag": {"tag": "biblScope", "attributes": {"unit": "issue"}},
    },
    "page_s": {
        "xpath": "imprint",
        "new_tag": {"tag": "biblScope", "attributes": {"unit": "pp"}},
    },
    "scientificEditor_s": {
        "xpath": "monogr",
        "new_tag": {"tag": "editor", "attributes": {}},
    },
    "publicationLocation_s": {
        "xpath": "imprint",
        "new_tag": {"tag": "pubPlace", "attributes": {}},
    },
    "publisher_s": {
        "xpath": "imprint",
        "new_tag": {"tag": "publisher", "attributes": {}},
    },
    "journalTitle_s": {
        "xpath": "monogr",
        "new_tag": {"tag": "title", "attributes": {"level": "j"}},
    },
    "bookTitle_s": {
        "xpath": "monogr",
        "new_tag": {"tag": "title", "attributes": {"level": "m"}},
    },
    "conferenceTitle_s": {
        "xpath": "meeting",
        "new_tag": {"tag": "title", "attributes": {}},
    },
    "city_s": {
        "xpath": "meeting",
        "new_tag": {"tag": "settlement", "attributes": {}},
    },
    "authorityInstitution_s": {
        "xpath": "monogr",
        "new_tag": {"tag": "authority", "attributes": {"type": "institution"}},
    },
}

TEI_TAGS_ORDER = {
    "imprint": ["publisher", "pubPlace", "biblScope", "date"],
    "meeting": ["title", "date", "settlement"],
    "monogr": [
        "idno",
        "title",
        "meeting",
        "respStmt",
        "settlement",
        "country",
        "editor",
        "imprint",
    ],
    "analytic": ["title", "author"],
}


def get_tei_notice_metadata_only(tei_str: str):
    soup = BeautifulSoup(tei_str, features="xml")
    soup.teiHeader.extract()
    soup.titleStmt.extract()
    soup.editionStmt.extract()
    soup.notesStmt.extract()
    soup.publicationStmt.extract()
    soup.seriesStmt.extract()
    soup.back.extract()
    return soup


def add_missing_fields(tei_str: str, fields: dict[str, str], doc_type: str) -> str:
    soup = get_tei_notice_metadata_only(tei_str)
    for field, value in fields.items():
        if field == "producedDateY_i":
            date_type = "datePub"
            if doc_type in ("HDR", "THESE"):
                date_type = "dateDefended"
            original_tag = soup.find_all("imprint")[0]
            new_tag = soup.new_tag("date", type=date_type)
            new_tag.string = value
            original_tag.append(new_tag)  # always last in imprint, so append is fine
            continue

        if field not in HAL_TO_TEI:
            raise KeyError("field not implemented in mapping yet")

        original_tag = soup.find_all(HAL_TO_TEI[field]["xpath"])[0]

        # TODO what if elem does not exist? create it from scratch
        new_tag = soup.new_tag(
            HAL_TO_TEI[field]["new_tag"]["tag"],
            **HAL_TO_TEI[field]["new_tag"]["attributes"]
        )
        new_tag.string = value

        tags_list = TEI_TAGS_ORDER[original_tag.name]
        tags_list = tags_list[: tags_list.index(new_tag.name)]
        if len(tags_list) == 0:  # tag is the first
            original_tag.insert(0, new_tag)
            continue

        # new_tag must find its place
        tags_list.reverse()
        is_tag_inserted = False
        for tag_name_before in tags_list:
            tags_before = [
                x for x in original_tag.children if tag_name_before == x.name
            ]
            if len(tags_before) == 0:
                continue
            tags_before[-1].insert_after(new_tag)
            is_tag_inserted = True
            break
        if not is_tag_inserted:
            original_tag.insert(0, new_tag)

    return str(soup)
