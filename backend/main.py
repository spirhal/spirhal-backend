import json
from typing import Set, Annotated
import requests
import logging
from contextlib import asynccontextmanager

from fastapi import FastAPI, Depends, HTTPException
from fastapi.staticfiles import StaticFiles
from fastapi.security import OAuth2PasswordBearer

from bs4 import BeautifulSoup
from backend.session import SessionCleaner, SessionManager
from backend.hal_to_tei_mappings import add_missing_fields
from backend.config import config

from backend.utils import display_and_value_name_from_string, normalize_string
from backend.models import (
    LoginInput,
    ResearcherListOutput,
    ResearcherListInput,
    ResearcherInfo,
    ResearcherNames,
    DuplicatesNoticesData,
    UpdateTeiData,
)

logger = logging.getLogger()

session_manager = SessionManager()


@asynccontextmanager
async def lifespan(app: FastAPI):
    logger.info("START scheduled task to clean sessions")
    session_cleaner = SessionCleaner(session_manager)
    session_cleaner.start()
    yield
    logger.info("STOP")


app = FastAPI(lifespan=lifespan)
api = FastAPI(lifespan=lifespan)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

app.mount("/api", api)
app.mount("/", StaticFiles(directory="static", html=True), name="front")


@api.get("/test-session")
def test_session(token: Annotated[str, Depends(oauth2_scheme)]):
    session = session_manager.get_session(token)
    return {"login": session["login"]}


@api.post("/login")
def connect(credentials: LoginInput):
    token = session_manager.get_new_session(credentials.login, credentials.password)
    return token


@api.get("/logout")
def disconnect(token: Annotated[str, Depends(oauth2_scheme)]):
    session_manager.delete_session(token)


@api.get("/labos")
def labos():
    with open("static/labos.json") as f:
        content = f.read()
        data = json.loads(content.encode().decode("utf-8-sig"))
    return data


@api.get("/researcher-list")
def researcher_list(
    query_data: ResearcherListInput = Depends(ResearcherListInput),
) -> ResearcherListOutput:
    labo_identifier = query_data.labo_identifier
    with open("static/labos.json") as f:
        content = f.read()
        data = json.loads(content.encode().decode("utf-8-sig"))
    labo = [x for x in data if x["name"].lower() == labo_identifier.lower()][0]
    researchers = {}
    duplicates = []
    for url in labo["url"]:
        html = requests.get(url)
        soup = BeautifulSoup(html.text, features="lxml")
        for ahref in soup.select(labo["querySelector"]):
            researcher_name = ahref.string
            link = ahref.attrs["href"]
            if not researcher_name or researcher_name.strip() == "":
                continue
            formatted_names = display_and_value_name_from_string(researcher_name)
            if any(
                [
                    formatted_names["dispName"] == r.name.dispName
                    for r in researchers.values()
                ]
            ):
                duplicates.append(formatted_names["dispName"])
            else:
                researchers[link] = ResearcherInfo(
                    originalName=researcher_name,
                    link=link,
                    name=ResearcherNames(**formatted_names),
                )

    return ResearcherListOutput(
        researchers=[value for value in researchers.values()],
        duplicates=duplicates,
    )


@api.post("/duplicate-notices")
def duplicates_notices(data: DuplicatesNoticesData) -> Set[str]:
    annuaire_html = requests.get(data.url)
    annuaire_text = BeautifulSoup(annuaire_html.text, features="lxml").get_text()
    page_text = normalize_string(annuaire_text)
    doubles = set()
    for notice in data.notices:
        if normalize_string(notice.title) in page_text:
            doubles.add(notice.id)
    return doubles


@api.post("/update-tei")
def update_tei(
    token: Annotated[str, Depends(oauth2_scheme)], data: UpdateTeiData
) -> str:
    session_data = session_manager.get_session(token)
    tei_url = f"{data.doc_url}/tei"
    tei_response = requests.get(tei_url)
    if not tei_response.ok:
        raise HTTPException(
            status_code=500, detail=f"TEI could not be retrieved at {tei_url}"
        )
    tei = tei_response.text

    # update TEI
    updated_tei = add_missing_fields(tei, data.fields, data.doc_type)

    # push TEI to HAL
    response_put = requests.put(
        f"{config.sword_api}{data.doc_halid}",
        data=updated_tei.encode("utf-8"),
        headers={
            "Packaging": "http://purl.org/net/sword-types/AOfr",
            "Content-Type": "text/xml",
        },
        auth=(session_data["login"], session_data["password"]),
    )
    if response_put.status_code == 400:
        raise HTTPException(
            status_code=400,
            detail="Call to SWORD API failed, server gave "
            f"{response_put.status_code},{response_put.text}",
        )
    if response_put.status_code == 401:
        raise HTTPException(
            status_code=401,
            detail="Authentication to SWORD API failed",
        )

    if not response_put.ok:
        print(updated_tei)
        raise HTTPException(
            status_code=500,
            detail="Call to SWORD API failed, server gave "
            f"{response_put.status_code},{response_put.text}",
        )

    return response_put.text
