from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    sword_api: str = "https://api.halpreprod.archives-ouvertes.fr/sword/"
    model_config = SettingsConfigDict(env_file=".env")


config = Settings()
