from typing import List, Dict
from fastapi import Query
from pydantic import BaseModel


class ResearcherNames(BaseModel):
    valName: str
    dispName: str


class ResearcherInfo(BaseModel):
    originalName: str
    link: str
    name: ResearcherNames


class Notice(BaseModel):
    id: str
    title: str


class ResearcherListInput(BaseModel):
    labo_identifier: str = Query(None)


class ResearcherListOutput(BaseModel):
    duplicates: List[str]
    researchers: List[ResearcherInfo]


class DuplicatesNoticesData(BaseModel):
    url: str
    notices: List[Notice]


class LoginInput(BaseModel):
    login: str
    password: str


class UpdateTeiData(BaseModel):
    doc_url: str
    doc_halid: str
    doc_type: str
    fields: Dict[str, str]
