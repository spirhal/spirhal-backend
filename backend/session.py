from datetime import datetime, timedelta
from threading import Thread
from time import sleep
from jose import JWTError, jwt
from fastapi import HTTPException


SECRET_KEY = "loutre"
ALGORITHM = "HS256"
TOKEN_VALIDITY_MINUTES = 120  # 2 hours


class SessionManager:
    def __init__(self, secret=SECRET_KEY, algorithm=ALGORITHM) -> None:
        self.sessions = {}
        self.secret = secret
        self.algorithm = algorithm

    def get_new_session(self, login, password):
        data: dict = {
            "login": login,
        }
        expire = datetime.utcnow() + timedelta(minutes=TOKEN_VALIDITY_MINUTES)
        data.update({"exp": expire})
        # TODO use the new SWORD login route when available
        # to validate the credentials
        encoded_jwt = jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)
        self.sessions[encoded_jwt] = {
            "password": password,
            **data,
        }
        return encoded_jwt

    def get_session(self, token):
        try:
            jwt.decode(token, SECRET_KEY, ALGORITHM)
            return self.sessions[token]
        except (JWTError, KeyError):
            raise HTTPException(status_code=403)

    def delete_session(self, token):
        try:
            jwt.decode(token, SECRET_KEY, ALGORITHM)
            del self.sessions[token]
        except (JWTError, KeyError):
            raise HTTPException(status_code=403)

    def clean_expired_session(self):
        now = datetime.utcnow()
        cleaned_sessions = {}
        for token, session in self.sessions.items():
            valid_token = False
            try:
                decoded = jwt.decode(token, key=SECRET_KEY, algorithms=ALGORITHM)
                if datetime.fromtimestamp(decoded["exp"]) > now:
                    valid_token = True
            except JWTError:
                pass
            if valid_token:
                cleaned_sessions[token] = session
            else:
                print(f"ERRASE SESSION: {session}")
        self.sessions = cleaned_sessions


class SessionCleaner(Thread):
    def __init__(self, session_manager: SessionManager):
        super().__init__()
        self.session_manager = session_manager

    def run(self):
        while True:
            print(len(self.session_manager.sessions))
            self.session_manager.clean_expired_session()
            sleep(60)
