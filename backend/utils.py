import unicodedata


def normalize_string(text: str) -> str:
    text = unicodedata.normalize("NFD", text)
    text = text.encode("ascii", "ignore")
    text = text.decode("utf-8").lower()
    return " ".join(text.split())  # normalize spacing


def display_and_value_name_from_string(original_name: str):
    tokens = original_name.split()
    firstname = ""
    lastname = ""
    stopwords = ["M.", "MME", "PR", "DR"]
    for token in tokens:
        if token in stopwords:
            continue
        # if token is in uppercase, it is probably the last name
        if token.upper() == token:
            lastname += token + " "
        else:
            firstname += token + " "
    # if there is no lastname founded then the last word is considered as lastname
    if not lastname:
        tokens = firstname.strip().split()
        lastname = " ".join(tokens[:-1])
        firstname = tokens[-1]
    lastname = lastname.title()
    return {
        "dispName": (lastname.strip() + " " + firstname.strip()),
        "valName": (firstname.strip() + " " + lastname.strip()),
    }
