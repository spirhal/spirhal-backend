###
FROM node:18-alpine AS front-base
FROM python:3.11 as back-base

### FRONT BUILDING ###

FROM front-base AS front-deps

RUN apk add --no-cache libc6-compat
WORKDIR /tmp/frontend
COPY ./frontend/package.json ./frontend/package-lock.json* ./
RUN npm ci

FROM front-base AS front-builder

WORKDIR /tmp/frontend
COPY --from=front-deps /tmp/frontend/node_modules ./node_modules
COPY ./frontend/ .
RUN npm run build:prod

### BACK BUILDING ###
FROM back-base as back-deps
WORKDIR /tmp
RUN pip install poetry
COPY ./pyproject.toml ./poetry.lock* /tmp/
RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

####

FROM back-base

WORKDIR /app

COPY static /app/static
COPY --from=front-builder /tmp/static /app/static
COPY --from=back-deps /tmp/requirements.txt /app/requirements.txt

RUN cat /app/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./backend /app/backend

CMD ["uvicorn", "backend.main:app", "--host", "0.0.0.0", "--port", "80"]
