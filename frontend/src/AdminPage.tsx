import React, { useCallback, useEffect, useState } from "react";
import { ResearcherSelector, getResearcher } from "./components/main/ResearcherSelector";
import { ResearcherAdmin } from "./components/main/ResearcherAdmin";
import { LaboAdmin } from "./components/main/LaboAdmin";
import { useSelectedLabo } from "./context/LaboContext";
import { Alert, AlertTitle, Box, Card, CardContent, CircularProgress, Container, Grid, Stack } from "@mui/material";

import { LaboCard } from "./components/common/LaboCard";
import { Researcher, useSelectedResearcher } from "./context/ResearcherContext";
import { Header } from "./components/common/Header";
import { Status } from "./common/Status";
import { getResearchers } from "./api";

interface AdminPageProps {
  onSwitchLabo?: () => void;
  onResearcherSelected: (researcher?: Researcher) => void;
  urlResearcherName?: string;
}

export function AdminPage({ onResearcherSelected, onSwitchLabo, urlResearcherName }: AdminPageProps) {
  const selectedLabo = useSelectedLabo();
  const researcher = useSelectedResearcher();
  const [researchers, setResearchers] = useState<Researcher[] | undefined>(undefined);
  const [duplicateResearchers, setDuplicateResearchers] = useState<string[]>([]);
  const [status, setStatus] = useState<Status>(Status.LOADING);

  const updateResearchers = useCallback(async () => {
    const { researchers, duplicates } = await getResearchers(selectedLabo);
    setResearchers(researchers);
    const selected = getResearcher(researchers, urlResearcherName);
    onResearcherSelected(selected);
    setDuplicateResearchers(duplicates);
    setStatus(Status.OK);
  }, []);

  useEffect(() => {
    updateResearchers();
  }, []);

  function getContent() {
    if (status === Status.LOADING) {
      return (
        <Stack alignItems={"center"}>
          <CircularProgress />
        </Stack>
      );
    } else if (researcher) {
      return <ResearcherAdmin researcher={researcher} />;
    } else if (!urlResearcherName) {
      return <LaboAdmin duplicateResearchers={duplicateResearchers} />;
    } else {
      return (
        <Alert severity="error">
          <AlertTitle>Chercheur⋅euse inconnu⋅e</AlertTitle>
          Impossible de trouver &quot;{urlResearcherName}&quot; dans l&apos;annuaire de {selectedLabo.name}
        </Alert>
      );
    }
  }

  return (
    <Stack spacing={2}>
      <Header onClickLogo={onSwitchLabo} />
      <Box alignItems={"center"}>
        <Container>
          <Stack spacing={2}>
            <Card>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} md={6} display={"flex"} justifyContent={"center"}>
                    <LaboCard labo={selectedLabo} onClickSwitch={onSwitchLabo} elevation={2} />
                  </Grid>
                  <Grid item xs={12} sm={12} md={6} display={"flex"} justifyContent={"center"}>
                    <ResearcherSelector
                      onResearcherSelected={onResearcherSelected}
                      researchers={researchers}
                      value={urlResearcherName}
                    />
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
            {getContent()}
          </Stack>
        </Container>
      </Box>
    </Stack>
  );
}
