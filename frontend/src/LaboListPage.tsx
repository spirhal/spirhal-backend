import React from "react";
import { Box, Container, Grid, Stack, Typography } from "@mui/material";

import { LaboCard } from "./components/common/LaboCard";
import { Labo, useLaboList } from "./context/LaboContext";
import { Header } from "./components/common/Header";

export interface LaboListPageProps {
  onLaboSelected: (labo: Labo) => void;
}

export function LaboListPage({ onLaboSelected }: LaboListPageProps) {
  const labos = useLaboList();
  return (
    <Stack spacing={2}>
      <Header />
      <Box alignItems={"center"}>
        <Container>
          <Stack spacing={2}>
            <Typography variant="h4">Sélectionner un laboratoire</Typography>
            <Grid container spacing={2}>
              {labos.map((labo) => (
                <Grid
                  item
                  xs={12}
                  md={6}
                  lg={4}
                  key={labo.name}
                  justifyContent="center"
                  alignItems="flex-start"
                  display="flex"
                >
                  <LaboCard labo={labo} onClick={() => onLaboSelected(labo)} />
                </Grid>
              ))}
            </Grid>
          </Stack>
        </Container>
      </Box>
    </Stack>
  );
}
