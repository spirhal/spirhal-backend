import React, { useCallback } from "react";
import { CircularProgress, Box, Paper } from "@mui/material";

import { LaboListPage } from "./LaboListPage";
import { Labo, LaboProvider } from "./context/LaboContext";
import { AdminPage } from "./AdminPage";
import { Researcher, ResearcherProvider } from "./context/ResearcherContext";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import { CustomThemeProvider } from "./context/DarkModeContext";
import { getLabosJson } from "./api";
import { LoginProvider } from "./context/LoginContext";

export const Admin: React.FC = () => {
  const [labos, setLabos] = React.useState<Labo[] | undefined>();
  const [selectedLabo, setSelectedLabo] = React.useState<Labo | undefined>();
  const [urlResearcherName, setUrlResearcherName] = React.useState<string | undefined>();
  const [selectedResearcher, setSelectedResearcher] = React.useState<Researcher | undefined>();

  const fetchLabos = useCallback(async () => {
    const fetchedLabos = await getLabosJson();
    setLabos(fetchedLabos);
    loadUrlParams(fetchedLabos);
  }, []);

  const loadUrlParams = useCallback((fetchedLabos: Array<Labo> | undefined) => {
    const urlSearchParam = new URLSearchParams(window.location.search);
    const urlParamSelectedLabo = urlSearchParam.get("labo");
    if (fetchedLabos !== undefined && urlParamSelectedLabo !== null) {
      const paramLabo = fetchedLabos.find((e: Labo) => e.name === urlParamSelectedLabo);
      if (paramLabo !== undefined) {
        setSelectedLabo(paramLabo);
        const urlParamSelectedResearcher = urlSearchParam.get("personne") ?? undefined;
        setUrlResearcherName(urlParamSelectedResearcher);
      }
    } else {
      setSelectedLabo(undefined);
    }
  }, []);

  React.useEffect(() => {
    fetchLabos();
  }, [fetchLabos]);

  React.useEffect(() => {
    // Make sure to read url params on history change
    window.onpopstate = () => loadUrlParams(labos);
  }, [labos]);

  function onLaboSelected(labo?: Labo) {
    setSelectedLabo(labo);
    setUrlResearcherName(undefined);
    updateUrlParams(labo, undefined);
  }

  function onResearcherSelected(researcher?: Researcher) {
    const urlName = researcher?.name.dispName;
    updateUrlParams(selectedLabo, urlName);
    setUrlResearcherName(urlName);
    setSelectedResearcher(researcher);
  }

  function updateUrlParams(labo?: Labo, researcher?: string) {
    const params = [];
    if (labo) {
      params.push(`labo=${labo.name}`);
    }
    if (researcher) {
      params.push(`personne=${researcher}`);
    }
    if (params.length > 0) {
      window.history.pushState("", "", `${window.location.pathname}?${params.join("&")}`);
    } else {
      window.history.pushState("", "", window.location.pathname);
    }
  }

  function getContent() {
    if (labos === undefined) {
      return (
        <Box display="flex" justifyContent="center" alignItems="center" height="100%">
          <CircularProgress />
        </Box>
      );
    }

    if (selectedLabo === undefined) {
      return <LaboListPage onLaboSelected={onLaboSelected} />;
    }

    return (
      <ResearcherProvider selectedResearcher={selectedResearcher}>
        <AdminPage
          onResearcherSelected={(r) => onResearcherSelected(r)}
          onSwitchLabo={() => onLaboSelected(undefined)}
          urlResearcherName={urlResearcherName}
        />
      </ResearcherProvider>
    );
  }
  return (
    <CustomThemeProvider>
      <LoginProvider>
        <LaboProvider laboList={labos ?? []} selectedLabo={selectedLabo}>
          <Paper elevation={0} square sx={{ minHeight: "100vh" }}>
            {getContent()}
          </Paper>
        </LaboProvider>
      </LoginProvider>
    </CustomThemeProvider>
  );
};
