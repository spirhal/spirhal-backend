import React, { useState } from "react";
import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import LoginIcon from "@mui/icons-material/Login";
import { LoadingButton } from "@mui/lab";
import { useSetJWT } from "../../context/LoginContext";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { loginUser } from "../../api";

export interface LoginModalProps {
  open: boolean;
  onCancel: () => void;
  onClosed?: () => void;
  onLoggedIn?: () => void;
}

interface FormInput {
  login: string;
  password: string;
}

export function LoginModal({ open, onCancel, onClosed, onLoggedIn }: LoginModalProps) {
  const {
    control,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<FormInput>();
  const [loading, setLoading] = useState(false);
  const setJWT = useSetJWT();

  const onSubmit: SubmitHandler<FormInput> = async (data) => {
    setLoading(true);
    try {
      const result = await loginUser(data.login, data.password);
      setLoading(false);
      if (result && typeof result === "string") {
        setJWT(result);
        if (onLoggedIn) {
          onLoggedIn();
        }
      } else {
        setError("root", { message: "Erreur inconnue" });
      }
    } catch (e) {
      setLoading(false);
      setError("root", { message: "Erreur inconnue" });
    }
  };

  function onClose() {
    if (!loading) {
      onCancel();
    }
  }

  return (
    <Dialog open={open} onClose={onClose} onTransitionExited={onClosed}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle>Connexion</DialogTitle>
        <DialogContent>
          <Stack spacing={2}>
            <Typography variant="body1">Merci de vous connecter pour modifier les informations sur HAL.</Typography>
            <Stack justifyContent={"center"} gap={1}>
              <Controller
                name="login"
                control={control}
                rules={{ required: { value: true, message: "Champs requis" } }}
                render={({ field, fieldState: { error } }) => (
                  <TextField
                    {...field}
                    label="Identifiant"
                    disabled={loading}
                    error={error !== undefined}
                    helperText={error?.message}
                  />
                )}
              />
              <Controller
                name="password"
                control={control}
                rules={{ required: { value: true, message: "Champs requis" } }}
                render={({ field, fieldState: { error } }) => (
                  <TextField
                    {...field}
                    label="Mot de passe"
                    type="password"
                    disabled={loading}
                    error={error !== undefined}
                    helperText={error?.message}
                  />
                )}
              />
            </Stack>
            {errors.root?.message ? <Alert severity="error">{errors.root?.message}</Alert> : null}
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} disabled={loading}>
            Annuler
          </Button>
          <LoadingButton type="submit" loading={loading} endIcon={<LoginIcon />} variant="contained">
            Se connecter
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}
