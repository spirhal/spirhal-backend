import React from "react";

import { Card, CardActionArea, CardMedia, CardContent, Typography, CardActions, Button } from "@mui/material";
import { Labo } from "../../context/LaboContext";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";

interface LaboCardProps {
  labo: Labo;
  onClick?: () => void;
  onClickSwitch?: () => void;
  elevation?: number;
}

export function LaboCard({ labo, onClick, onClickSwitch, elevation }: LaboCardProps) {
  return (
    <Card sx={{ width: 345 }} elevation={elevation}>
      {onClick ? (
        <CardActionArea onClick={onClick}>
          <LaboCardContent labo={labo} />
        </CardActionArea>
      ) : (
        <LaboCardContent labo={labo} onClickSwitch={onClickSwitch} />
      )}
    </Card>
  );
}

export function LaboCardContent({ labo, onClickSwitch }: Omit<LaboCardProps, "onClick">) {
  return (
    <>
      <CardMedia
        component="img"
        height="140px"
        width="250px"
        sx={{ objectFit: "contain" }}
        image={labo.logo}
        alt={`${labo.name} logo`}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {labo.name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Norme choisie: {labo.norm}
        </Typography>
      </CardContent>
      {onClickSwitch ? (
        <CardActions>
          <Button size="small" onClick={onClickSwitch} startIcon={<ArrowBackIcon />}>
            Changer de labo
          </Button>
          <Button
            size="small"
            onClick={() => {
              window.open(labo.url[0]);
            }}
            startIcon={<LibraryBooksIcon />}
          >
            Annuaire
          </Button>
        </CardActions>
      ) : null}
    </>
  );
}
