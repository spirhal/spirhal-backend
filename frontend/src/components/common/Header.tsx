import React, { useState } from "react";
import { AppBar, Box, Button, Container, IconButton, Link, Stack, Toolbar, Typography, useTheme } from "@mui/material";
import { useToggleDarkMode } from "../../context/DarkModeContext";
import DarkMode from "@mui/icons-material/DarkMode";
import LightMode from "@mui/icons-material/LightMode";

import spirhalImg from "../../images/SPIRHAL.png";
import { LoginModal } from "./LoginModal";
import { useIsLoggedIn, useJWT, useSetJWT } from "../../context/LoginContext";
import { logoutUser } from "../../api";

interface HeaderProps {
  onClickLogo?: () => void;
}

export function Header({ onClickLogo }: HeaderProps) {
  const toggleDarkMode = useToggleDarkMode();
  const theme = useTheme();
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const loggedIn = useIsLoggedIn();
  const setJWT = useSetJWT();
  const jwt = useJWT();

  return (
    <Stack spacing={2}>
      <AppBar position="static">
        <Toolbar>
          <Stack direction={"row"} spacing={2} flexGrow={1} alignItems={"center"}>
            <img
              width={80}
              src={spirhalImg}
              onClick={onClickLogo}
              style={{
                cursor: "pointer",
              }}
            />
            <Typography
              variant="h6"
              component="div"
              sx={{
                display: { xs: "none", sm: "none", md: "block" },
              }}
            >
              Interface de gestion des publications
            </Typography>
          </Stack>
          <IconButton onClick={toggleDarkMode}>
            {theme.palette.mode === "light" ? <LightMode /> : <DarkMode />}
          </IconButton>
          <Button
            onClick={() => {
              if (!loggedIn) {
                setLoginModalOpen(true);
              } else {
                logoutUser(jwt ?? "");
                setJWT(undefined);
              }
            }}
            variant={loggedIn ? "contained" : "text"}
            color={loggedIn ? "error" : "primary"}
          >
            {loggedIn ? "Déconnexion" : "Connexion"}
          </Button>
        </Toolbar>
      </AppBar>
      <Box alignItems={"center"}>
        <Container>
          <Typography variant="body1">
            Ce module permet d&apos;homogénéiser vos références bibliographiques entre votre fiche annuaire (sur le site
            web de votre laboratoire) et vos documents hébergés sur HAL. Il s&apos;agit ici de supprimer les doublons
            présents sur votre fiche annuaire et de compléter les{" "}
            <Link href="https://fr.wikipedia.org/wiki/M%C3%A9tadonn%C3%A9e">métadonnées</Link>, c&apos;est-à-dire les
            contenus des champs manquants de vos documents hébergés sur HAL.
          </Typography>
        </Container>
      </Box>
      <LoginModal
        open={loginModalOpen}
        onCancel={() => setLoginModalOpen(false)}
        onLoggedIn={() => setLoginModalOpen(false)}
      />
    </Stack>
  );
}
