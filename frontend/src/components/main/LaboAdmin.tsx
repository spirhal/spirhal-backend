import React from "react";
import { Alert, AlertTitle, Card, CardContent, Stack } from "@mui/material";

interface LaboAdminProps {
  duplicateResearchers: string[];
}

export function LaboAdmin({ duplicateResearchers }: LaboAdminProps) {
  return (
    <Card>
      <CardContent>
        <Stack spacing={1}>
          <Alert severity="info">
            <AlertTitle>Aucun⋅e chercheur⋅euse sélectionné⋅e</AlertTitle>
            Utiliser le menu déroulant en haut de la page pour séléctionner un⋅e chercheur⋅euse
          </Alert>
          {duplicateResearchers.length > 0 ? (
            <Alert severity="warning">
              <AlertTitle>
                Certains chercheur⋅euse⋅s apparaissent plusieurs fois dans l&apos;annuaire du laboratoire
              </AlertTitle>
              {duplicateResearchers?.join(", ")}
            </Alert>
          ) : null}
        </Stack>
      </CardContent>
    </Card>
  );
}
