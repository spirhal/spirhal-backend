import React, { useEffect, useState } from "react";
import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useNoticeCheck } from "../../../context/NoticeChecksContext";
import { getFieldTranslation } from "../../../utils/stringUtils";
import { LoadingButton } from "@mui/lab";
import { DUPLICATE_TAG } from "../../../utils/normCheck";
import { Notice } from "spirhal";
import { Controller, FormProvider, SubmitHandler, useForm, useFormContext } from "react-hook-form";
import { ApiError, logoutUser, sendEdits } from "../../../api";
import { useJWT, useSetJWT } from "../../../context/LoginContext";

export interface EditModalProps {
  open: boolean;
  notice: Notice;
  onCancel: () => void;
  onSaved: (changedData: FormInput) => void;
  onClosed: () => void;
}

type FormInput = Record<string, string>;

export function EditModal({ open, notice, onCancel, onSaved, onClosed }: EditModalProps) {
  const formData = useForm<FormInput>();
  const {
    handleSubmit,
    setError,
    reset,
    formState: { errors },
  } = formData;
  const [loading, setLoading] = useState(false);
  const checks = useNoticeCheck(notice);
  const jwt = useJWT();
  const setJWT = useSetJWT();

  useEffect(() => {
    reset({});
  }, [notice]);

  const onSubmit: SubmitHandler<FormInput> = async (data) => {
    setLoading(true);
    try {
      await sendEdits(jwt ?? "", notice, data);
      setLoading(false);
      onSaved(data);
    } catch (e) {
      setLoading(false);
      if (e instanceof ApiError) {
        // Logout users on 401
        if (e.status === 401) {
          logoutUser(jwt ?? "");
          setJWT(undefined);
        } else {
          console.error(e);
          setError("root", { message: "Erreur inconnue" });
        }
      } else {
        console.error(e);
        setError("root", { message: "Erreur inconnue" });
      }
    }
  };

  function onClose() {
    if (!loading) {
      onCancel();
    }
  }

  const fields = checks.filter((c) => c !== DUPLICATE_TAG);

  return (
    <Dialog open={open} onClose={onClose} onTransitionExited={onClosed}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormProvider {...formData}>
          <DialogTitle>{notice.title_s ?? "Titre"}</DialogTitle>
          <DialogContent sx={{ position: "relative" }}>
            <Stack spacing={2}>
              <Typography variant="body1">
                Merci de renseigner les champs manquants. Cliquez sur &quot;sauvegarder&quot; pour envoyer les
                modifications sur HAL.
              </Typography>
              {fields.map((c, i) => (
                <FormItem key={i} name={c} disabled={loading} />
              ))}
              {errors.root?.message ? <Alert severity="error">{errors.root?.message}</Alert> : null}
            </Stack>
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} disabled={loading}>
              Annuler
            </Button>
            <LoadingButton type="submit" loading={loading} variant="contained">
              Sauvegarder
            </LoadingButton>
          </DialogActions>
        </FormProvider>
      </form>
    </Dialog>
  );
}

function FormItem({ name, disabled }: { name: string; disabled: boolean }) {
  const { control } = useFormContext();
  let type = "text";
  if (name.endsWith("_i")) {
    type = "number";
  }
  return (
    <Controller
      name={name}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <TextField
          {...field}
          label={getFieldTranslation(name)}
          type={type}
          disabled={disabled}
          error={error !== undefined}
          helperText={error?.message}
        />
      )}
    />
  );
}
