import React, { useRef } from "react";
import { EditModal } from "./EditModal";
import { Notice } from "spirhal";
import { LoginModal } from "../../common/LoginModal";
import { useIsLoggedIn } from "../../../context/LoginContext";

interface NoticeModalProps {
  notice?: Notice;
  onClose: () => void;
  onSaved: (changedData: Record<string, string>) => void;
}

// To show/hide the modal, we set/unset the notice, which is very convenient
// because the UI inside the modal is generated using this data.
// As there is a transition while hiding the modal,
// the UI breaks when we set the notice the undefined.
// This hook retains the notice's value until the transition is finished
// This way we have a clean transition without worrying about this from
// outside the component
function useKeepNoticeDuringTransition(notice?: Notice) {
  const noticeRef = useRef<Notice | undefined>(undefined);
  if (notice !== undefined) {
    noticeRef.current = notice;
  }

  function onTransitionEnd() {
    if (notice === undefined) {
      noticeRef.current = undefined;
    }
  }

  return { persistentNotice: noticeRef.current, onTransitionEnd };
}

export function NoticeModal({ notice, onClose, onSaved }: NoticeModalProps) {
  const { persistentNotice, onTransitionEnd } = useKeepNoticeDuringTransition(notice);
  const open = notice !== undefined;

  const loggedIn = useIsLoggedIn();

  if (loggedIn && persistentNotice) {
    return (
      <EditModal
        open={open}
        notice={persistentNotice}
        onCancel={onClose}
        onSaved={onSaved}
        onClosed={onTransitionEnd}
      />
    );
  }
  return <LoginModal open={open} onCancel={onClose} onClosed={onTransitionEnd} />;
}
