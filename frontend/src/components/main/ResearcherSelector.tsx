import React from "react";
import { useSelectedLabo } from "../../context/LaboContext";
import { Alert, AlertTitle, Autocomplete, CircularProgress, Link, Stack, TextField, Typography } from "@mui/material";
import { Researcher } from "../../context/ResearcherContext";

export interface ResearcherSelectorProps {
  onResearcherSelected: (researcher?: Researcher) => void;
  researchers?: Researcher[];
  value?: string;
}

export function ResearcherSelector({ onResearcherSelected, researchers, value }: ResearcherSelectorProps) {
  const loading = researchers === undefined;
  const labo = useSelectedLabo();

  return (
    <Stack spacing={1}>
      <Stack spacing={1} display={"flex"} alignItems={"center"}>
        <Typography>Sélectionner chercheur⋅euse:</Typography>
        <Autocomplete
          sx={{ width: 300 }}
          value={value ? { label: value } : undefined}
          options={researchers?.map((r) => ({ label: r.name.dispName })) ?? []}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Chercheur⋅euse"
              value={value}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                ),
              }}
            />
          )}
          loading={loading}
          onChange={(_event, value) => {
            if (value === null || researchers === undefined) {
              onResearcherSelected(undefined);
            } else {
              const selected = researchers.find((r) => r.name.dispName === value.label);
              console.log(researchers);
              if (!selected) {
                console.warn("Unknown researcher selected");
              } else {
                onResearcherSelected(getResearcher(researchers, value.label));
              }
            }
          }}
        />
      </Stack>
      {researchers !== undefined && researchers.length === 0 ? (
        <Alert severity="warning">
          <AlertTitle>Aucun⋅e chercheur⋅euse trouvé⋅e</AlertTitle>
          Merci de vérifier que{" "}
          {labo.url.map((url, i) => (
            <Link key={i} href={url}>
              {url}
            </Link>
          ))}{" "}
          pointe vers l&apos;annuaire du laboratoire
        </Alert>
      ) : null}
    </Stack>
  );
}

export function getResearcher(researchers: Researcher[], dispName?: string) {
  const selected = researchers.find((r) => r.name.dispName === dispName);
  if (!selected) {
    console.warn("Unknown researcher selected");
    return undefined;
  }
  return selected;
}
