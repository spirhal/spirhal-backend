import React from "react";
import { useContext } from "react";
import { Alert, Button, Card, CardContent, Stack, Typography } from "@mui/material";
import { useNoticeChecks } from "../../context/NoticeChecksContext";
import { computeDetails } from "../../utils/normCheck";
import { useSelectedResearcher } from "../../context/ResearcherContext";
import StickyNote2Icon from "@mui/icons-material/StickyNote2";
import ViewListIcon from "@mui/icons-material/ViewList";
import { NormContext, Notice } from "spirhal";
import { useSelectedLabo } from "../../context/LaboContext";

export interface AdminDetails {
  nbPubli: number;
  nbValidPubli: number;
  nbFullText: number;
  nbDoubles: number;
  nbMissings: number;
}

interface ResearcherRecapProps {
  notices: Notice[];
}

export function ResearcherRecap({ notices }: ResearcherRecapProps) {
  const norm = useContext(NormContext);

  const checks = useNoticeChecks();
  const recap = computeDetails(notices, checks);
  const researcher = useSelectedResearcher();
  const labo = useSelectedLabo();

  const { nbPubli, nbDoubles, nbFullText, nbMissings, nbValidPubli } = recap;
  const nbNotices = nbPubli - nbFullText;

  function s(num: number) {
    return num > 1 ? "s" : "";
  }

  return (
    <Card>
      <CardContent>
        <Stack spacing={1} alignItems={"center"}>
          <Typography variant="h4">{researcher?.name.dispName}</Typography>
          <Stack direction={"row"} spacing={1}>
            <Button
              onClick={() => {
                if (researcher?.link) {
                  window.open(researcher?.link);
                }
              }}
              startIcon={<StickyNote2Icon />}
            >
              Page annuaire
            </Button>
            <Button
              onClick={() => {
                if (researcher) {
                  let url = `https://univ-tlse2.halpreprod.archives-ouvertes.fr/search/index?q=${researcher.name.valName}`;
                  const structIdList = labo.struct_id.map((s) => `structId_i:${s}`);
                  url += ` AND (${structIdList.join(" OR ")})`;
                  window.open(url);
                }
              }}
              startIcon={<ViewListIcon />}
            >
              Liste des publications sur HAL
            </Button>
          </Stack>
          <Stack spacing={1} width={"100%"}>
            <Alert severity="info">
              Il y a <strong>{nbPubli}</strong> document{s(nbPubli)} présent{s(nbPubli)} dans HAL (
              <strong>{nbNotices}</strong> notice{s(nbNotices)} et <strong>{nbFullText}</strong> texte
              {s(nbFullText)} {nbFullText > 1 ? "intégraux" : "intégral"}).
            </Alert>
            {nbValidPubli > 0 ? (
              <Alert severity="success">
                Il y a <strong>{nbValidPubli}</strong> document{s(nbValidPubli)} présent{s(nbValidPubli)} dans HAL,
                complet
                {s(nbValidPubli)} et conforme{s(nbValidPubli)} au style bibliographique {norm}.
              </Alert>
            ) : null}
            {nbMissings > 0 ? (
              <Alert severity="warning">
                Il y a <strong>{nbMissings}</strong> champ{s(nbMissings)} vide{s(nbMissings)} dans le
                {s(nbMissings)} document{s(nbMissings)} présent{s(nbMissings)} dans HAL.
              </Alert>
            ) : null}
            {nbDoubles > 0 ? (
              <Alert severity="error">
                Il y a <strong>{nbDoubles}</strong> doublon{s(nbDoubles)} (documents identiques dans HAL et sur la fiche
                annuaire du chercheur).
              </Alert>
            ) : null}
          </Stack>
        </Stack>
      </CardContent>
    </Card>
  );
}
