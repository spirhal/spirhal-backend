import React, { useEffect, useState } from "react";
import { Labo, useSelectedLabo } from "../../context/LaboContext";
import { SpirhalAdminGroup } from "./notices/SpiralAdminGroup";
import { ResearcherRecap } from "./ResearcherRecap";
import { DUPLICATE_TAG, NoticesTags, checkNotice } from "../../utils/normCheck";
import { Alert, AlertTitle, CircularProgress, Stack } from "@mui/material";
import { NoticeChecksProvider } from "../../context/NoticeChecksContext";
import { Researcher, useSelectedResearcher } from "../../context/ResearcherContext";
import { NoticeModal } from "./modal/NoticeModal";
import { Status } from "../../common/Status";
import { HALGroup, NormContext, Notice, useFilteredHalGroups, useHALGroups } from "spirhal";
import { getDuplicates } from "../../api";

export interface ResearcherAdminProps {
  researcher: Researcher;
}

function useGroups(selectedLabo: Labo, researcher: Researcher) {
  const [groupsSatus, originalGroups] = useHALGroups(
    "",
    selectedLabo.struct_id,
    10000,
    researcher.name.valName,
    undefined,
    "https://api.halpreprod.archives-ouvertes.fr"
  );
  const [filteredGroups, setFilteredGroups] = useState<HALGroup[]>([]);
  const [status, setStatus] = useState(Status.OK);

  useEffect(() => {
    setFilteredGroups(useFilteredHalGroups(originalGroups));
    setStatus(groupsSatus);
  }, [groupsSatus, originalGroups]);

  const updateNotice = (docId: string | undefined, data: Record<string, string>) => {
    const newFilteredGroups = [...filteredGroups];
    newFilteredGroups.forEach((g) => {
      const editedNotice = g.notices.find((n) => n.docid === docId);
      for (let key of Object.keys(data)) {
        // @ts-ignore
        editedNotice[key] = data[key];
      }
    });
  };

  return { status, groups: filteredGroups, updateNotice };
}

export function ResearcherAdmin({ researcher }: ResearcherAdminProps) {
  const selectedLabo = useSelectedLabo();
  const { status, groups, updateNotice } = useGroups(selectedLabo, researcher);
  const notices = groups.map((g) => g.notices).flat();
  const [detailsStatus, checks] = useAdminChecks(notices, status, groups);
  const [noticeEditing, setNoticeEditing] = useState<Notice | undefined>();

  if (status === Status.ERROR || detailsStatus === Status.ERROR) {
    return (
      <Alert severity="error">
        <AlertTitle>Une erreur est survenue</AlertTitle>
        Merci de contacter le support.
      </Alert>
    );
  }

  if (status === Status.LOADING || detailsStatus === Status.LOADING) {
    return (
      <Stack alignItems={"center"}>
        <CircularProgress />
      </Stack>
    );
  }

  return (
    <NormContext.Provider value={selectedLabo.norm}>
      <NoticeChecksProvider checks={checks}>
        <NoticeModal
          notice={noticeEditing}
          onClose={() => setNoticeEditing(undefined)}
          onSaved={(data) => {
            // Locally update the list of notices so we dont refetch the data
            updateNotice(noticeEditing?.docid, data);
            setNoticeEditing(undefined);
          }}
        />
        <ResearcherRecap notices={notices} />
        {groups.map((group, i) => (
          <SpirhalAdminGroup key={i} group={group} onPressEdit={setNoticeEditing} />
        ))}
      </NoticeChecksProvider>
    </NormContext.Provider>
  );
}

function useAdminChecks(notices: Notice[], groupsStatus: Status, filteredGroups: HALGroup[]): [Status, NoticesTags] {
  const [checks, setChecks] = useState<NoticesTags>({});
  const [status, setStatus] = useState<Status>(Status.LOADING);
  const researcher = useSelectedResearcher();
  useEffect(() => {
    async function fetchDetails() {
      setStatus(Status.LOADING);
      try {
        if (!researcher?.link) {
          throw new Error("Uknown researcher url");
        }
        const duplicates = await getDuplicates(researcher, notices);
        const tags: NoticesTags = {};
        for (const d of duplicates) {
          tags[d] = [DUPLICATE_TAG];
        }
        for (let n of notices) {
          const missingFields = checkNotice(n);
          if (missingFields.length > 0) {
            if (tags[n.docid]) {
              tags[n.docid].push(...missingFields);
            } else {
              tags[n.docid] = missingFields;
            }
          }
        }
        setChecks(tags);
        setStatus(Status.OK);
      } catch (e) {
        console.error(e);
        setStatus(Status.ERROR);
      }
    }
    if (groupsStatus === Status.OK) {
      fetchDetails();
    } else if (groupsStatus !== status) {
      // Reset checks on error/loading
      setStatus(groupsStatus);
      setChecks({});
    }
  }, [groupsStatus, filteredGroups]);

  return [status, checks];
}
