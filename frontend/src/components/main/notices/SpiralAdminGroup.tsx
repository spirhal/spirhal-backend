import React from "react";
import { Box, List, Typography } from "@mui/material";
import { SpirhalAdminNotice } from "./SpiralAdminNotice";
import { HALGroup, Notice } from "spirhal";

export interface SpirhalAdminGroupProps {
  group: HALGroup;
  onPressEdit: (notice: Notice) => void;
}

export function SpirhalAdminGroup({ group, onPressEdit }: SpirhalAdminGroupProps) {
  const { notices, name } = group;
  return (
    <Box>
      <Typography variant="h6">{name}</Typography>
      <List>
        {notices.map((notice, i) => (
          <SpirhalAdminNotice key={i} notice={notice} onPressEdit={onPressEdit} />
        ))}
      </List>
    </Box>
  );
}
