import React from "react";
import { Box, Button, Chip, IconButton, Link, ListItem, ListItemIcon, ListItemText, Stack } from "@mui/material";
import DownloadIcon from "@mui/icons-material/PictureAsPdfOutlined";
import CheckIcon from "@mui/icons-material/TaskAltOutlined";
import WarningIcon from "@mui/icons-material/WarningAmberOutlined";
import ErrorIcon from "@mui/icons-material/ErrorOutline";
import EditIcon from "@mui/icons-material/Edit";
import { useNoticeCheck } from "../../../context/NoticeChecksContext";
import { isDuplicateTag } from "../../../utils/normCheck";
import { getFieldTranslation } from "../../../utils/stringUtils";
import { GenericNoticeComponent, Notice, NoticeTypeContext } from "spirhal";

export interface SpirhalAdminNoticeProps {
  notice: Notice;
  onPressEdit: (notice: Notice) => void;
}

export function SpirhalAdminNotice({ notice, onPressEdit }: SpirhalAdminNoticeProps) {
  const noticeType = notice.docType_s;
  const checks = useNoticeCheck(notice);
  const hasDuplicate = checks.filter(isDuplicateTag).length > 0;
  const hasMissingFields = checks.filter((t) => !isDuplicateTag(t)).length > 0;
  let icon = <CheckIcon color="success" />;
  if (hasDuplicate) {
    icon = <ErrorIcon color="error" />;
  } else if (hasMissingFields) {
    icon = <WarningIcon color="warning" />;
  }
  return (
    <NoticeTypeContext.Provider value={{ noticeType }}>
      <ListItem
        secondaryAction={
          notice.files_s !== undefined && notice.files_s.length > 0 ? (
            <IconButton edge="end" aria-label="comments" href={notice.files_s[0]}>
              <DownloadIcon color="error" />
            </IconButton>
          ) : undefined
        }
      >
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText
          primary={
            <Link href={notice.uri_s}>
              <GenericNoticeComponent {...notice} />
            </Link>
          }
          secondary={
            hasMissingFields || hasDuplicate ? (
              <Stack spacing={1} direction={"row"} alignItems={"center"} flexWrap={"wrap"}>
                {hasMissingFields ? (
                  // Make sure to only display the fix button if there are missing fields
                  // The form will not do anything if there is only a duplicate
                  <Box paddingTop={1}>
                    <Button
                      size="small"
                      color="warning"
                      onClick={() => onPressEdit(notice)}
                      startIcon={<EditIcon />}
                      variant="outlined"
                    >
                      Corriger
                    </Button>
                  </Box>
                ) : null}
                {checks.map((field, i) => (
                  <Box key={i} paddingTop={1}>
                    <Chip
                      label={getFieldTranslation(field)}
                      color={isDuplicateTag(field) ? "error" : "warning"}
                      variant="outlined"
                      size="small"
                    />
                  </Box>
                ))}
              </Stack>
            ) : undefined
          }
        />
      </ListItem>
    </NoticeTypeContext.Provider>
  );
}
