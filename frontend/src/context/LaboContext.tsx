import React, { PropsWithChildren, useContext } from "react";

export interface Labo {
  name: string;
  logo: string;
  url: string[];
  querySelector: string;
  struct_id: number[];
  norm: string;
}

export interface LaboContextType {
  laboList: Array<Labo>;
  selectedLabo?: Labo;
}

const LaboContext = React.createContext<LaboContextType>({ laboList: [] });

export function LaboProvider({ laboList, selectedLabo, children }: PropsWithChildren<LaboContextType>) {
  return <LaboContext.Provider value={{ laboList, selectedLabo }}>{children}</LaboContext.Provider>;
}

export function useLaboList() {
  const { laboList } = useContext(LaboContext);
  return laboList;
}

export function useSelectedLabo() {
  const { selectedLabo } = useContext(LaboContext);
  if (!selectedLabo) {
    throw Error("No labo selected");
  }
  return selectedLabo;
}
