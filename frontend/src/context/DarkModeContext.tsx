import { ThemeProvider, createTheme, useMediaQuery } from "@mui/material";
import React, { PropsWithChildren, useContext, useState } from "react";

export interface CustomThemeType {
  toggleDarkMode?: () => void;
}

const CustomThemeContext = React.createContext<CustomThemeType>({ toggleDarkMode: () => undefined });

const DARK_MODE_KEY = "spirhal.admin.dark";

export function CustomThemeProvider({ children }: PropsWithChildren<{}>) {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
  const savedDarkMode = localStorage.getItem(DARK_MODE_KEY) === "1";
  const hasSavedDarkMode = localStorage.getItem(DARK_MODE_KEY) !== null;

  const initialDarkMode = hasSavedDarkMode ? savedDarkMode : prefersDarkMode;

  const [darkMode, setDarkMode] = useState(initialDarkMode);

  function toggleDarkMode() {
    localStorage.setItem(DARK_MODE_KEY, darkMode ? "0" : "1");
    setDarkMode(!darkMode);
  }

  const defaultTheme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode: darkMode ? "dark" : "light",
        },
      }),
    [darkMode]
  );

  const theme = React.useMemo(
    () =>
      createTheme(defaultTheme, {
        components: {
          MuiAppBar: {
            styleOverrides: {
              colorPrimary: {
                backgroundColor: defaultTheme.palette.background.paper,
                color: defaultTheme.palette.text.primary,
              },
            },
          },
        },
      }),
    [defaultTheme]
  );

  return (
    <CustomThemeContext.Provider value={{ toggleDarkMode }}>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </CustomThemeContext.Provider>
  );
}

export function useToggleDarkMode() {
  const { toggleDarkMode } = useContext(CustomThemeContext);
  return toggleDarkMode;
}
