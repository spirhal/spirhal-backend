import React, { PropsWithChildren, useContext } from "react";

export interface Researcher {
  originalName: string;
  name: {
    dispName: string;
    valName: string;
  };
  link: string | null;
}

export interface ResearcherContextType {
  selectedResearcher?: Researcher;
}

const ResearcherContext = React.createContext<ResearcherContextType>({});

export function ResearcherProvider({ selectedResearcher, children }: PropsWithChildren<ResearcherContextType>) {
  return <ResearcherContext.Provider value={{ selectedResearcher }}>{children}</ResearcherContext.Provider>;
}

export function useSelectedResearcher() {
  const { selectedResearcher } = useContext(ResearcherContext);
  return selectedResearcher;
}
