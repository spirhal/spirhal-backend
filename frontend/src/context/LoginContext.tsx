import React, { PropsWithChildren, useContext, useState } from "react";

export interface LoginContextType {
  jwt?: string;
  setJWT: (jwt?: string) => void;
}

const LoginContext = React.createContext<LoginContextType>({ setJWT: () => undefined });

export function LoginProvider({ children }: PropsWithChildren<Omit<LoginContextType, "setJWT">>) {
  const [jwt, setJWT] = useState<string | undefined>(undefined);
  return <LoginContext.Provider value={{ jwt, setJWT }}>{children}</LoginContext.Provider>;
}

export function useIsLoggedIn() {
  const { jwt } = useContext(LoginContext);
  return jwt !== undefined;
}

export function useJWT() {
  const { jwt } = useContext(LoginContext);
  return jwt;
}

export function useSetJWT() {
  const { setJWT } = useContext(LoginContext);
  return setJWT;
}
