import React, { PropsWithChildren, useContext } from "react";
import { NoticesTags } from "../utils/normCheck";
import { Notice } from "spirhal";

export interface NoticeChecksContextType {
  checks: NoticesTags;
}

const NoiceChecksContext = React.createContext<NoticeChecksContextType>({ checks: {} });

export function NoticeChecksProvider({ checks, children }: PropsWithChildren<NoticeChecksContextType>) {
  return <NoiceChecksContext.Provider value={{ checks }}>{children}</NoiceChecksContext.Provider>;
}

export function useNoticeChecks() {
  const { checks } = useContext(NoiceChecksContext);
  return checks;
}
export function useNoticeCheck(notice?: Notice) {
  const { checks } = useContext(NoiceChecksContext);
  if (notice) {
    return checks[notice.docid] ?? [];
  } else {
    return [];
  }
}
