import { Notice } from "spirhal";
import { Labo } from "./context/LaboContext";
import { Researcher } from "./context/ResearcherContext";

function getApiUrl(path: string) {
  return `/api/${path}`;
}

export class ApiError extends Error {
  private _status: number;

  constructor(status: number, message: string) {
    super(message);
    this._status = status;
    this.message = message;
  }

  get status() {
    return this._status;
  }
}

async function fetchAPI(path: string, init?: Parameters<typeof fetch>[1]) {
  const result = await fetch(getApiUrl(path), init);
  if (result.status === 200) {
    const jsonData = await result.json();
    return jsonData;
  }
  const body = await result.text();
  throw new ApiError(result.status, body);
}

export async function getLabosJson(): Promise<Labo[]> {
  return fetchAPI("labos");
}

export function getResearchers(labo: Labo): Promise<{ duplicates: string[]; researchers: Researcher[] }> {
  const params = new URLSearchParams({ labo_identifier: labo.name });
  return fetchAPI("researcher-list?" + params);
}

export function getDuplicates(researcher: Researcher, notices: Notice[]): Promise<Array<string>> {
  return fetchAPI("duplicate-notices", {
    method: "POST",
    body: JSON.stringify({
      url: researcher.link,
      notices: notices.map((n) => ({ id: n.docid, title: n.title_s[0] })),
    }),
    headers: { "Content-Type": "application/json" },
  });
}

export function loginUser(login: string, password: string): Promise<string> {
  return fetchAPI("login", {
    method: "POST",
    body: JSON.stringify({
      login,
      password,
    }),
    headers: { "Content-Type": "application/json" },
  });
}

export function logoutUser(jwt: string): Promise<void> {
  return fetchAPI("logout", {
    method: "GET",
    headers: { "Content-Type": "application/json", Authorization: `Bearer ${jwt}` },
  });
}

export function sendEdits(jwt: string, notice: Notice, fields: Record<string, string>): Promise<{ status: boolean }> {
  return fetchAPI("update-tei", {
    method: "POST",
    body: JSON.stringify({
      doc_url: notice.uri_s,
      doc_halid: notice.halId_s,
      doc_type: notice.docType_s,
      fields,
    }),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwt}`,
    },
  });
}
