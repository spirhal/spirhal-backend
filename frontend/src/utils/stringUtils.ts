import { Notice } from "spirhal";

function stripAccent(str: string) {
  var accent = [
    /[\300-\306]/g,
    /[\340-\346]/g, // A, a
    /[\310-\313]/g,
    /[\350-\353]/g, // E, e
    /[\314-\317]/g,
    /[\354-\357]/g, // I, i
    /[\322-\330]/g,
    /[\362-\370]/g, // O, o
    /[\331-\334]/g,
    /[\371-\374]/g, // U, u
    /[\321]/g,
    /[\361]/g, // N, n
    /[\307]/g,
    /[\347]/g, // C, c
  ];
  var noaccent = ["A", "a", "E", "e", "I", "i", "O", "o", "U", "u", "N", "n", "C", "c"];

  for (var i = 0; i < accent.length; i++) {
    str = str.replace(accent[i], noaccent[i]);
  }
  return str;
}

function removeBrackets(str: string) {
  return str.replace(/\s*\(.[^(]*\)/g, "");
}

export function clearString(s: string) {
  //var ret = s.toLowerCase().sansAccent().removeBrackets().trim();
  let ret = s.toLowerCase().replace(/â\u0080/g, "");
  ret = stripAccent(ret);
  ret = removeBrackets(ret);
  return ret.replace(/-/g, "").trim();
}

const NOTICE_TRANSLATIONS: Partial<Record<keyof Notice, string>> = {
  title_s: "Titre",
  producedDateY_i: "Date",
  publicationLocation_s: "Lieu de publication",
  publisher_s: "Editeur Commercial",
  journalTitle_s: "Titre du journal",
  volume_s: "Volume",
  issue_s: "Numéro",
  scientificEditor_s: "Editeur scientifique",
  bookTitle_s: "Titre de l'ouvrage",
  page_s: "Pages",
  conferenceTitle_s: "Nom de la conférence",
  city_s: "Ville",
  authorityInstitution_s: "Institution",
};

export function getFieldTranslation(field: string) {
  const translation = NOTICE_TRANSLATIONS[field as keyof Notice];
  if (translation === undefined) {
    return field;
  }
  return translation;
}
