import { Notice } from "spirhal";

export type NoticesTags = Record<string, string[]>;
export const DUPLICATE_TAG = "DOUBLON";

export function computeDetails(notices: Notice[], tags: NoticesTags) {
  let nbPubli = 0;
  let nbValidPubli = 0;
  let nbFullText = 0;
  let nbDoubles = 0;
  let nbMissings = 0;
  notices.forEach((notice) => {
    nbPubli++;
    if (notice.files_s && notice.files_s.length > 0) {
      nbFullText++;
    }
    const noticeTags = tags[notice.docid];
    if (noticeTags && noticeTags.length > 0) {
      noticeTags.forEach((tag) => {
        if (tag === DUPLICATE_TAG) {
          nbDoubles++;
        } else {
          nbMissings++;
        }
      });
    } else {
      nbValidPubli++;
    }
  });
  return {
    nbPubli: nbPubli,
    nbValidPubli: nbValidPubli,
    nbFullText: nbFullText,
    nbDoubles: nbDoubles,
    nbMissings: nbMissings,
  };
}

export function isDuplicateTag(tag: string) {
  return tag === DUPLICATE_TAG;
}

export function checkNotice(notice: Notice) {
  const missingFields: Set<string> = new Set();

  function updateMissingFields(key: keyof Notice) {
    if (!notice[key]) {
      missingFields.add(key);
    }
  }
  const type = notice.docType_s;
  switch (type) {
    case "OUV":
      updateMissingFields("title_s");
      updateMissingFields("producedDateY_i");
      updateMissingFields("publicationLocation_s");
      updateMissingFields("publisher_s");
      break;
    case "DOUV":
      updateMissingFields("title_s");
      updateMissingFields("producedDateY_i");
      if (notice.journalTitle_s) {
        updateMissingFields("producedDateY_i");
        updateMissingFields("journalTitle_s");
        updateMissingFields("volume_s");
        updateMissingFields("issue_s");
      } else if (notice.bookTitle_s) {
        updateMissingFields("scientificEditor_s");
        updateMissingFields("bookTitle_s");
        updateMissingFields("publisher_s");
        updateMissingFields("page_s");
        updateMissingFields("publicationLocation_s");
      } else {
        updateMissingFields("publicationLocation_s");
        updateMissingFields("publisher_s");
        updateMissingFields("page_s");
      }
      break;
    case "COUV":
      updateMissingFields("title_s");
      updateMissingFields("producedDateY_i");
      updateMissingFields("scientificEditor_s");
      updateMissingFields("bookTitle_s");
      updateMissingFields("publisher_s");
      updateMissingFields("page_s");
      updateMissingFields("publicationLocation_s");
      break;
    case "ART":
      updateMissingFields("title_s");
      updateMissingFields("producedDateY_i");
      updateMissingFields("journalTitle_s");
      updateMissingFields("volume_s");
      updateMissingFields("issue_s");
      updateMissingFields("page_s");
      break;
    case "COMM":
      updateMissingFields("title_s");
      updateMissingFields("producedDateY_i");
      updateMissingFields("conferenceTitle_s");
      updateMissingFields("city_s");
      updateMissingFields("publisher_s");
      updateMissingFields("page_s");
      break;
    case "POSTER":
      updateMissingFields("title_s");
      updateMissingFields("producedDateY_i");
      updateMissingFields("conferenceTitle_s");
      updateMissingFields("city_s");
      updateMissingFields("publisher_s");
      break;
    case "THESE":
    case "HDR":
      updateMissingFields("title_s");
      updateMissingFields("producedDateY_i");
      updateMissingFields("authorityInstitution_s");
      break;
  }

  return [...missingFields];
}
