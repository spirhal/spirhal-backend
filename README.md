# Spirhal Backend

This repository contains the SpirHAL admin backend and frontend.
The admin frontend needs a backend so parsing of laboratory pages is not done in the frontend. This improves performance and prevents issues with CORS.

## Installing

### Manual

To install the project manually, you will need to build both backend and frontend.

#### Frontend

1. Install NodeJS ([doc](https://nodejs.org/en/learn/getting-started/how-to-install-nodejs))
2. Install JavaScript dependencies
    ```bash
    npm i
    ```
3. Build the production app
    ```bash
    npm run build:prod
    ```

You now have a static website built into the `./static` folder.
See the next section to install the backend and deploy the frontend.

#### Backend

1. Install Poetry ([doc](https://python-poetry.org/docs/#installation))
   - Poetry can be installed in a dedicated venv for the project or with a
global installation. See the Poetry documentation for more information.
2. Install python dependencies
    ```bash
    poetry install
    ```
3. Launch the server
    ```bash
    poetry run uvicorn backend.main:app
    ```
Along with some API routes, this will serve the contents of the `./static` folder, where the frontend build output should be.

### Docker

The docker install contains a fully working app, with both backend and frontend ready.

1. Install Docker ([doc](https://python-poetry.org/docs/#installation))
2. Build image
    ```bash
    docker build . -r spirhal-backend
    ```
3. Launch image
    ```bash
    docker run -p 8080:80 spirhal-backend
    ```
The app should be available on http://localhost:8080

## Contributing

To contribute to the API, you only need to install the backend.
To contribute to the frontend, you need to install both backend and frontend.

### Installation

#### Frontend

Follow the regular installation process, but use the following command to build the project:
```bash
npm run build:watch
```
Then install and run the backend.

#### Backend

Install the project with dev dependencies

```
poetry install --with=dev
```

Launch server with auto reload

```bash
poetry run uvicorn backend.main:app --reload
```

### Tests

#### Frontend

There are currently no tests.

#### Backend

Run tests with

```
poetry run pytest
```
